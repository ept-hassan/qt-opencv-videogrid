#include "mainwindow.h"
#include <QApplication>
#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include<opencv2/opencv.hpp>
#include <opencv2/videoio/videoio.hpp>


using namespace cv;
using namespace std;
//using namespace cv;

int main(int argc, char *argv[])
{
//    cv::Mat image = cv::Mat::zeros(100, 100, CV_8UC3);

//    cv::imshow("image", image);
//    cv::waitKey(10);
    VideoCapture Video1("/home/hassan/Downloads/Countdown8.mp4");
    VideoCapture Video2("/home/hassan/Downloads/Countdown8.mp4");
    VideoCapture Video3("/home/hassan/Downloads/Countdown8.mp4");
    VideoCapture Video4("/home/hassan/Downloads/Countdown8.mp4");

    int VideoWidth = 640;
    int VideoHeight = 360;

    Mat Video1Frame, Video2Frame, Video3Frame, Video4Frame; // Creating Mat objects to store the captured frames
    Mat Video1Shrunk, Video2Shrunk, Video3Shrunk, Video4Shrunk; //Creating Mat objects to store the shrunk frames
    Mat FullCombinedVideo(VideoWidth,VideoHeight, Video1Shrunk.type()); //Creating Mat object to store the combined video
    Mat CombinedVideoTop(VideoWidth, VideoHeight/2, Video1Shrunk.type());
    Mat CombinedVideoBottom(VideoWidth, VideoHeight/2, Video1Shrunk.type());


    double Video1FrameCount = Video1.get(CAP_PROP_FPS);
    double Video2FrameCount = Video2.get(CAP_PROP_FPS);
    double Video3FrameCount = Video3.get(CAP_PROP_FPS);
    double Video4FrameCount = Video4.get(CAP_PROP_FPS);

    double CombinedFrameTopCount = min(Video1FrameCount, Video2FrameCount);
    double CombinedFrameBottomCount = min(Video3FrameCount, Video4FrameCount);
    double CombinedFrameCount = min(CombinedFrameBottomCount, CombinedFrameTopCount);

    Video1.set(CAP_PROP_FPS, CombinedFrameCount);
    Video2.set(CAP_PROP_FPS, CombinedFrameCount);
    Video3.set(CAP_PROP_FPS, CombinedFrameCount);
    Video4.set(CAP_PROP_FPS, CombinedFrameCount);
    for(;;)
       {
           Video1 >> Video1Frame;
           Video2 >> Video2Frame;
           Video3 >> Video3Frame;
           Video4 >> Video4Frame;

           resize(Video1Frame, Video1Shrunk, Size(100, 100));
           resize(Video2Frame, Video2Shrunk, Size(100, 100));
           resize(Video3Frame, Video3Shrunk, Size(100, 100));
           resize(Video4Frame, Video4Shrunk, Size(100, 100));

//           resize(Video1Frame, Video1Shrunk, Size((VideoWidth/2), (VideoHeight/2)));
//           resize(Video2Frame, Video2Shrunk, Size((VideoWidth/2), (VideoHeight/2)));
//           resize(Video3Frame, Video3Shrunk, Size((VideoWidth/2), (VideoHeight/2)));
//           resize(Video4Frame, Video4Shrunk, Size((VideoWidth/2), (VideoHeight/2)));

           Video1Shrunk.copyTo(CombinedVideoTop(Rect(0,0,Video1Shrunk.cols,Video1Shrunk.rows)));
           Video2Shrunk.copyTo(CombinedVideoTop(Rect(Video1Shrunk.cols,0,Video2Shrunk.cols,Video2Shrunk.rows)));
           Video3Shrunk.copyTo(CombinedVideoBottom(Rect(0,0,Video3Shrunk.cols,Video3Shrunk.rows)));
           Video4Shrunk.copyTo(CombinedVideoBottom(Rect(Video3Shrunk.cols,0,Video4Shrunk.cols,Video4Shrunk.rows)));

           CombinedVideoTop.copyTo(FullCombinedVideo(Rect(0,0,CombinedVideoTop.cols, CombinedVideoTop.rows)));
           CombinedVideoBottom.copyTo(FullCombinedVideo(Rect(0,CombinedVideoTop.rows, CombinedVideoBottom.cols,CombinedVideoBottom.rows)));
           imshow("Frame", FullCombinedVideo);

//           std::string FinalFilePath = CombinedVideo.toUtf8().constData();
//           cv::VideoWriter CombinedWriter(FinalFilePath, CV_FOURCC('D', 'I', 'V', 'X'),10,cvSize(FullCombinedVideo.cols, FullCombinedVideo.rows));

//           CombinedWriter << FullCombinedVideo;

       }

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
