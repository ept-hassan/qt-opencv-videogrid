#-------------------------------------------------
#
# Project created by QtCreator 2023-03-27T22:32:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += /usr/local/include/opencv4
LIBS += -L/usr/local/lib -lopencv_stitching -lopencv_highgui -lopencv_core -lopencv_imgproc -lopencv_videoio

TARGET = untitled1
TEMPLATE = app



SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
